<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package BubbleScribble
 */
?>
<div class="sidebar">
		<?php if ( is_active_sidebar('sidebar-widget-area') ) : ?>
		<?php dynamic_sidebar('sidebar-widget-area'); ?>
		<?php endif; ?>
</div>