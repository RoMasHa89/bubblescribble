<?php
/**
 * The template for displaying all pages.
 *
 * @package BubbleScribble
 */
 get_header(); ?>
 <?php while (have_posts()) : the_post(); ?>
 		<div id="content" class="content">
			<div class="content_resize">
				<div class="mainbar">
					<div class="page_section">
						<article class="single_post">
							<div class="article">
								<h2><?php the_title(); ?></h2>
								<?php the_content(); ?>
								<div class="clear"></div>
								<p><?php posts_nav_link(); ?></p>
							</div>
							<div class="comments">
								<?php comments_template(); ?>
							</div>
							<div class="clear"></div>
						</article>
					 </div>
				</div>
				<?php  get_sidebar(); ?>
				<div class="clr"></div>
			</div>
		</div>
<?php endwhile; ?>
<?php get_footer(); ?>