<?php
/**
 *
 * @package BubbleScribble
 */
?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<div class="article_text">
		<h2><a href="<?php the_permalink() ?>"><?php if(get_the_title($post->ID)) { the_title(); } else { the_time( get_option( 'date_format' ) ); } ?></a></h2>
		<p><span class="date"><?php the_time( get_option( 'date_format' ) ); ?></span> &nbsp;|&nbsp; <?php _e( 'Posted by', 'bubblescribble' ); ?> <?php echo the_author_link();?> &nbsp;|&nbsp; <?php _e( 'Category', 'bubblescribble' ); ?> <?php the_category(', '); ?></p>
		<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
			<a class="article_img" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('news-bubblescribble'); ?></a>
		<?php endif; ?>
		<?php the_excerpt(); ?>
			<p class="spec">
				<a href="<?php the_permalink(); ?>" class="rm"><?php _e( 'Read More', 'bubblescribble' ); ?></a>
				<a href="<?php echo get_comments_link( $post->ID ); ?> " class="com"><?php _e( 'Comments', 'bubblescribble' ); ?> (<?php $coms = wp_count_comments( $post->ID ); echo $coms->approved; ?>)</a>
			</p>
	</div>
	<div class="clear"></div>
</article>