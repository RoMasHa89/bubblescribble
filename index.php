<?php
/**
 * The main template file.
 *
 * @package BubbleScribble
 */
get_header(); ?>
	<div id="content" class="content">
		<div class="content_resize">
			<div class="mainbar">
				<div class="article">
					<div class="page_section">
						<?php while (have_posts()) : the_post(); ?>
							<?php get_template_part( 'content', 'posts');  ?>
						<?php endwhile; ?>
						<span class="prev"><?php next_posts_link(__('Previous Posts', 'bubblescribble')) ?></span>
						<span class="next"><?php previous_posts_link(__('Next posts', 'bubblescribble')) ?></span>
					</div>
				</div>
			</div>
			<?php  get_sidebar(); ?>
			<div class="clr"></div>
		</div>
	</div>
<?php get_footer(); ?>