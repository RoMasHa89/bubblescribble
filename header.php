<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package BubbleScribble
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_uri(); ?>" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="main">
		<div class="header" >
			<div class="header_resize">
				<header id="header" class="container clearfix">
					<nav class="menu_nav">
						   <?php wp_nav_menu( array('container'=> '', 'items_wrap'  => '<ul>%3$s</ul>'  ) ); ?>
					</nav>
				</header>
			<div class="clr"></div>
			</div>
		</div>
		<div class="hbg">
			<div class="logo">
				<h1><span><a href="<?php get_home_url(); ?>"><?php echo get_bloginfo( 'name' );?></span><small><?php echo get_bloginfo( 'description' );?></small></a></h1>
			</div>
		</div>
