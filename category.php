<?php
/**
 * The template for displaying category
 *
 *
 * @package BubbleScribble
 */
get_header(); ?>
		<div id="content" class="content">
			<div class="content_resize">
				<div class="mainbar">
					<div class="inner_page clearfix sidebar_right">
						<div class="page_section">
							<h2><?php printf( __( 'Category Archives: %s', 'bubblescribble' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h2>
							<?php while (have_posts()) : the_post(); ?>
								<?php get_template_part( 'content', 'posts');  ?>
							<?php endwhile; ?>
							<span class="prev"><?php next_posts_link(__('Previous Posts', 'bubblescribble')) ?></span>
							<span class="next"><?php previous_posts_link(__('Next posts', 'bubblescribble')) ?></span>
						</div>

					</div>
				</div>
				<?php  get_sidebar(); ?>
				<div class="clr"></div>
			</div>
		</div>
<?php get_footer(); ?>