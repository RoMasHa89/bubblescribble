<?php
/**
 * The template for displaying page NOT FOUND.
 *
 * @package BubbleScribble
 */
 get_header(); ?>
<div id="content" class="content">
	<div class="content_resize">
		<div class="mainbar">
			<div class="article">
				<div class="page_section">
					<h2><?php _e( 'Not found', 'bubblescribble' ); ?></h2>
					<p><?php _e( 'Sorry, but you are looking for something that isn\'t here.', 'bubblescribble' ); ?></p>
				</div>
			</div>
		</div>
		<?php  get_sidebar(); ?>
		<div class="clr"></div>
	</div>
</div>

<?php get_footer(); ?>
