<?php
/**
 * The template for displaying search
 *
 * @package BubbleScribble
 */
get_header(); ?>
<div id="content" class="content">
	<div class="content_resize">
		<div class="mainbar">
			<div class="page_section">
				<article class="single_post">
					<div class="article">
						<h2><?php printf( __( 'Search Results for: %s', 'bubblescribble' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
						<?php while (have_posts()) : the_post(); ?>
							<?php get_template_part( 'content', 'posts');  ?>
						<?php endwhile; ?>
						<hr class="separe" />
						<span class="prev"><?php next_posts_link(__('Previous Posts', 'bubblescribble')) ?></span>
						<span class="next"><?php previous_posts_link(__('Next posts', 'bubblescribble')) ?></span>
				</article>
			</div>
		</div>
		<?php  get_sidebar(); ?>
		<div class="clr"></div>
	</div>
</div>
<?php get_footer(); ?>

