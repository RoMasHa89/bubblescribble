<?php
/**
 * The template for displaying the footer.
 *
 *
 * @package BubbleScribble
 */
?>
		<footer id="footer">
			<div class="fbg">
				<div class="fbg_resize">
					<div class="footer_widgets columnwrapp clearfix">
						<div class="column4">
							<?php if ( is_active_sidebar('footer-widget-area') ) : ?>
							<?php dynamic_sidebar('footer-widget-area'); ?>
							<?php endif; ?>
						</div>
					<div class="clr"></div>
				</div>
			</div>
			<div class="footer">
				<div class="footer_resize">
					<p class="lf"><?php _e( 'Copyright', 'bubblescribble' ); ?> &copy; 2015 <a href="<?php get_home_url(); ?>"><?php echo get_bloginfo( 'name' );?></a> - <?php _e( 'All Rights Reserved', 'bubblescribble' ); ?></p>
					<p class="rf"><a href="#">WP Templates</a> by <a href="http://web4pro.net/">WEB4PRO</a></p>
					<div class="clr"></div>
				</div>
			</div>
		</footer> 
	</div>
<?php wp_footer(); ?>		
</body>
</html>