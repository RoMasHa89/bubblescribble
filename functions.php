<?php
/**
 * BubbleScribble functions and definitions
 *
 * @package BubbleScribble
 */

if ( ! function_exists( 'bubblescribble_setup' ) ) :
function bubblescribble_setup() {
    global $content_width;
	if ( ! isset( $content_width ) ) { $content_width = 1000; }
	load_theme_textdomain( 'bubblescribble', get_template_directory() . '/languages' );

	register_nav_menu( 'main', __( 'Main Menu', "bubblescribble" ) );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );	
	add_theme_support( 'custom-background' );	

	set_post_thumbnail_size( 150, 150, true );
	add_image_size( 'news-bubblescribble', 200, 200, true );
	add_image_size( 'large-feature-bubblescribble', 600, 480, true );
	add_image_size( 'small-feature-bubblescribble', 500, 300 );

}
endif; 
add_action( 'after_setup_theme', 'bubblescribble_setup' );


function bubblescribble_ie_support_header() {
    echo '<!--[if lt IE 9]>'. "\n";
    echo '<script src="' . esc_url( get_template_directory_uri() . '/js/html5.js' ) . '"></script>'. "\n";
    echo '<![endif]-->'. "\n";
}
add_action( 'wp_head', 'bubblescribble_ie_support_header', 1 );

if ( ! function_exists( 'bubblescribble_widgets_init' ) ) :
function bubblescribble_widgets_init() {
	register_sidebar(array(
		'name' => __( 'Sidebar Widget Area', "bubblescribble"),
		'id' => 'sidebar-widget-area',
		'description' => __( 'The sidebar widget area', "bubblescribble"),
		'before_widget' => '<aside id="%1$s" class="gadget %2$s"> ',
		'after_widget' => '</aside>',
		'before_title' => '<h2 class="star">',
		'after_title' => '</h2>',
	));		
	register_sidebar(array(
		'name' => __( 'Footer Widget Area', "bubblescribble"),
		'id' => 'footer-widget-area',
		'description' => __( 'The footer widget area', "bubblescribble"),
		'before_widget' => '<aside id="%1$s" class="col c1 %2$s"> ',
		'after_widget' => '</aside>',
		'before_title' => '<h2 class="widget-title">',
		'after_title' => '</h2>'
	));
}
endif;
add_action( 'widgets_init', 'bubblescribble_widgets_init' );

?>