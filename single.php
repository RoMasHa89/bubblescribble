<?php
/**
 * The template for displaying all pages.
 *
 * @package BubbleScribble
 */
 get_header(); ?>
 <?php while (have_posts()) : the_post(); ?>
 		<div id="content" class="content">
			<div class="content_resize">
				<div class="mainbar">
					<div class="article">
						<div class="page_section">
								<article class="single_post">
									<div class="article_text">
										<h2><?php if(get_the_title($post->ID)) { the_title(); } else { the_time( get_option( 'date_format' ) ); } ?></h2>
										<p><span class="date"><?php the_time( get_option( 'date_format' ) ); ?></span> &nbsp;|&nbsp; <?php _e( 'Posted by', 'bubblescribble' ); ?> <?php echo the_author_link();?> &nbsp;|&nbsp; <?php _e( 'Category', 'bubblescribble' ); ?> <?php the_category(', '); ?></p>
										<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
												<?php the_post_thumbnail($post->ID, 'featured'); ?>
										<?php endif; ?>
										<?php the_content(); ?>
										<p class="meta_tags"><?php the_tags(); ?></p>
										<p><?php posts_nav_link(); ?></p>
									</div>
									<?php
									// Previous/next post navigation.
									the_post_navigation( array(
											'next_text' => '<span class="next">' . __( 'Next post:', 'bubblescribble' )  .
													' %title</span>',
											'prev_text' => '<span class="prev">' . __( 'Previous post:', 'bubblescribble' ) .
													' %title</span>',
									) );

									// End the loop.
									?>
									<div class="clr"></div>

									<div class="form">
											<?php comments_template(); ?>
									</div>
								</article>
						 </div>
					</div>
				</div>
				<?php  get_sidebar(); ?>
				<div class="clr"></div>
			</div>
		</div>
<?php endwhile; ?>
<?php get_footer(); ?>