<?php
/**
 *
 * @package BubbleScribble
 */
get_header();
if ( have_posts() ) :
if ( 'posts' == get_option( 'show_on_front')) {
?>
		<div id="content" class="content">
			<div class="content_resize">
				<div class="mainbar">
					<div class="article">
						<div class="page_section">
							<!-- <h2><?php _e( 'Blog', 'bubblescribble' ); ?></h2><hr> -->
							<?php while (have_posts()) : the_post(); ?>
								<?php get_template_part( 'content', 'posts');  ?>
							<?php endwhile; ?>
							<!-- <hr class="separe" /> -->
							<span class="prev"><?php next_posts_link(__('Previous Posts', 'bubblescribble')) ?></span>
							<span class="next"><?php previous_posts_link(__('Next posts', 'bubblescribble')) ?></span>
						</div>
					</div>
				</div>
			<?php  get_sidebar(); ?>
			<div class="clr"></div>
			</div>
		</div>
<?php } else { ?>
        <?php
		    get_template_part( 'content', 'home' );
        ?>
<?php }
endif;
get_footer(); ?>